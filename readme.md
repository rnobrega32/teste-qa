# Prova QA
# Apêndice
- [Cenários de teste](#cenários-de-teste)
- [Consultas SQL](#consultas-sql)
- [Automação api Trello](#automação-api-trello)

# Cenários de teste


**1 - Cenário** – Validar apresentação da mensagem de erro ao não preencher o Campo 'Cliente destino' 


Dado Que estou na tela de transferência de valores
E clico no campo 'Cliente destino'
Quando não digitar a informação no campo 'Cliente destino'
E clicar no botão 'Transferir'
Então apresentar uma mensagem de erro


**2 - Cenário** - Validar Data de efetivação erro ao não preencher o Campo 'Data de efetivação'


Dado Que estou na tela de transferência de valores
E clico no campo 'Data de efetivação'
Quando não digitar a informação no campo 'Data de efetivação'
E clicar no botão 'Transferir'
Então apresentar uma mensagem de erro
 


**3 - Cenário** - Validar campo Valor erro ao não preencher o Campo 'Valor'

Dado Que estou na tela de transferência de valores
E clico no campo 'Valor'
Quando não digitar a informação no campo 'Valor'
E clicar no botão 'Transferir'
Então apresentar uma mensagem de erro
 


**4 - Cenário** - Validar Botão Transferir mensagem de sucesso ao clicar no botão 'Transferir'

Dado Que estou na tela de transferência de valores
Quando preencher todos os campos obrigatórios
E clicar no botão 'Transferir'
Então apresentar uma mensagem de 'Sucesso'


**5 - Cenário** - Validar Botão Cancelar ao preencher todos os campos obrigatórios sou redirecionado para a tela inicial 

Dado Que estou na tela de transferência de valores
Quando preencher todos os campos obrigatórios
E clicar no botão 'Cancelar'
Então o usuário é redirecionado para a tela inicial


**6 - Cenário** - Validar Saldo na Tela transferência de valores entre conta correntes

Dado Que estou na tela de transferência de valores entre conta correntes
Quando existir saldo 
Então apresentar o saldo na tela


**7 - Cenário** - Validar se o saldo foi debitado após a transferência de valores entre conta correntes
 

Dado Que estou na tela de transferência de valores entre conta correntes
Quando uma transação for realizada com sucesso 
Então validar se o valor foi debitado da conta corrente


**8 - Cenário** - Validar o campo valor mensagem de erro ao não preencher o Campo 'Valor' 

Dado Que estou na tela de transferência de valores
E clico no campo 'Valor'
Quando digitar uma cadeia de caracteres 
E clicar no botão 'Transferir'
Então apresentar uma mensagem de erro campo invalido 


**9 - Cenário** - Validar Data de efetivação mensagem de erro ao não preencher o Campo 'Data de efetivação' 

Dado Que estou na tela de transferência de valores
E clico no campo 'Data de efetivação'
Quando digitar a informação no campo 'Data de efetivação'
E clicar no botão 'Transferir'
Então apresentar uma mensagem de erro data invalida

# Consultas SQL

Listar nomes de todos os alunos matriculados na disciplina Cálculo do professor João.
```sql 
select 
	a.nome 
from cursa c 
inner join aluno on c.coda = a.coda
inner join disciplina d on d.codd = c.codd
inner join professor p on p.codp = d.codp
where d.nome = 'Cálculo' and p.nome = 'João';
``` 
Exibir quantidade de alunos por disciplinas.
```sql 
select 
	d.nome, count(a.coda) as total_alunos
from cursa c 
inner join aluno on c.coda = a.coda
inner join disciplina d on d.codd = c.codd
group by d.codd;
``` 
Listar disciplina que todos os professores lecionam.
```sql 
select 
	p.nome,
	d.nome
from disciplina d
inner join professor p on p.codp = d.codp
order by d.nome;
``` 
Total de professores.
```sql 
select count(1) as total from professor; 
``` 
Alunos que cursaram alguma disciplina entre os anos 2000 e 2020.
```sql 
select a.nome 
from cursa c 
inner join aluno a on a.coda = c.coda
where c.ano between 2000 and 2020;
``` 
# Automação api Trello

## Mapa mental
![Mapa mental](assets/images/mapa_mental.jpg)

## Teste de api utilizando cypress
Para executar o teste é necessário um token e uma key válidas do trello ([documentação](https://developer.atlassian.com/cloud/trello/guides/rest-api/api-introduction/#managing-your-api-key)). 

Tanto o token quanto a key devem ser adicionadas no arquivo ```cypress.env.json```

```json
{
    "token": "",
    "key": ""
}
```

Instalando dependências
```
npm install
```
Executando cypress
```
npx cypress open
```

