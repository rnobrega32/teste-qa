
context('Testa api trello', () => {
    
    
    const {token, key} = Cypress.env()
    let boardId = null
    let listId = null
    let cardId = null

    it('trello - cadastrar board', () => {
        const name = 'teste de board api'
        const qs = {key, token, name}
        cy.request({method: 'POST', url: 'boards', qs}).then(response => {
            expect(response.status).to.eq(200)
            expect(response.body).to.contain({name})
            boardId = response.body.id;
            cy.log(Cypress.config(), response.body)
        })
    })

    it('trello - criar list na board', () => {
        const name = 'lista de teste'
        const qs = {key, token, name}
        cy.request({method: 'POST', url: `boards/${boardId}/lists`, qs}).then(response => {
            expect(response.status).to.eq(200)
            expect(response.body).to.contain({name})
            listId = response.body.id;
            cy.log(Cypress.config(), response.body)
        })
    })

    it('trello - cadastrar card na board', () => {
        const name = 'card de teste'
        const qs = {key, token, idList: listId, name}
        cy.request({method: 'POST', url: 'cards', qs}).then(response => {
            expect(response.status).to.eq(200)
            expect(response).property('body').to.contain({name})
            cardId = response.body.id;
            cy.log(Cypress.config(), response.body)
        })
    })

   

    it('trello - excluir card na board', () => {        
        const qs = {key, token}
        cy.request({method: 'DELETE', url: `cards/${cardId}`, qs}).then(response => {
            expect(response.status).to.eq(200)
            cy.log(Cypress.config(), response.body)
        })
    })

    it('trello - excluir board', () => {        
        const qs = {key, token}
        cy.request({method: 'DELETE', url: `boards/${boardId}`, qs}).then(response => {
            expect(response.status).to.eq(200)
            cy.log(Cypress.config(), response.body)
        })
    })
    
})